#! /usr/bin/python3

from stravalib.client import Client
from sync import Sync
import time

class Strava(Sync, Client):
    def __init__(self):
        Client.__init__(self)
        Sync.__init__(self)


    def connect(self):
        if self.token['refresh_token'] and time.time() > self.token['expires_at']:
            token = self.refresh_access_token(client_id = self.client_id,
                                      client_secret = self.client_secret,
                                      refresh_token = self.token['refresh_token'])
            print (token)
            self.write()

    def get_permission_url(self):
        # self.s = Client(self.client_id, self.client_secret)
        auth_url = self.authorization_url(
                client_id = self.client_id,
                redirect_uri = 'http://localhost:8282/authorized',
                scope = ['read_all','profile:read_all','activity:read_all'])
        return auth_url

    def get_permission_token(self, code):
        token_response = self.exchange_code_for_token(
                client_id = self.client_id,
                client_secret = self.client_secret,
                code = code)
        return token_response
