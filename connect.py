#! /usr/bin/python3

import json, os, sys
import urllib.parse as urlparse

class Connect():
    def __init__(self, client_id = None, client_secret = None,
                token = None, config = None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        #TODO: read as variables
        self.client = None #If i use injeritence it will be self
        self.connected = False

        if not config:
            current_dir = os.path.dirname(os.path.realpath(sys.argv[0]))
            c = ''.join(['$XDG_CONFIG_HOME','/{}.config'.format(self.__class__.__bases__[0].__name__)])
            config = os.path.expandvars(c)
            self.client_id = None
            self.client_secret = None
            self.token = None
        self.config = config

        self.auth_method = "url" #code or url

        self.read()

        self.connection()

    def connection(self):
        print("Connection",self.connected)
        self.connect()
        print("Connection",self.connected)
        if not self.connected:
            self.notify_connection()

    def connect(self):
        # Call the connect fucntion or initialize client object
        # Should also handle refreshing
        pass

    def info_check(self):
        pass

    def notify_connection(self):
        print('We have not connection')
        print('Please act accordingly')
        self.on_authorize()

    def on_authorize(self):
        self.authorize()

    def authorize(self):
        # Should hadle first time
        self.request_permission()

        self.receive_permission()
        self.write()

    #@request_permission_url
    def request_permission(self, *args):
        url = self.get_permission_url()
        self.request_permission_url(url)

    def receive_permission(self, *args):
        code = self.get_permission_code()
        self.token = self.get_permission_token(code)

    def get_permission_code(self):
        #This is a mess
        if self.auth_method == "url":
            url = self.receive_url()
            parsed = urlparse.urlparse(url)
            code = urlparse.parse_qs(parsed.query)
        elif self.auth_method == "code":
            code = self.receive_code()
        return code

    #gui
    def receive_code(self, *args):
        code = input("Waiting for the code\n")
        return code

    #gui
    def receive_url(self, *args):
        url = input("Waiting for the url\n")
        return url

    #gui
    def request_permission_url(self, url):
        print("Go to {}".format(url))

    def request_client_info(self, *args, **kwargs):
        self.receive_client_info(*args, **kwargs)

    # @request_client_info
    def receive_client_info(self, *args, **kwargs):
        self.client_id = input("Waiting for ClientID\n")
        self.client_secret = input("Waiting for ClientSecret\n")

    # @request_permission
    def get_permission_url(self, auth):
        #Return the client.auth_url function
        pass

    #TODO: This is confusing do I need 3 functions
    #probably won't work either
    def get_permission_token(self, code):
        # Return the client.response
        pass

    def read(self,*args):
        try:
            f = open(self.config, 'r')
            data = json.load(f)
            self.client_id, self.client_secret, self.token, *args = data.values()
            f.close()
        except (IOError, json.decoder.JSONDecodeError) as e:
            print('No configuration file',e)
            # self.request_client_info()

    def write(self):
        f = open(self.config, 'w')
        data = {'client_id':self.client_id,
                'client_secret':self.client_secret,
                'token':self.token,
                'auth_method':self.auth_method}
        json.dump(data, f)
        f.close()

    def delete(self):
        os.remove(self.config)

    def with_connection(func,*args):
        def check(self, *args):
            try:
                r = func(self, *args)
                self.info_check()
                self.connected = True
                return r
            except Exception as e:
                self.connected = False
                print ('Connection failed:', e)
        return check
