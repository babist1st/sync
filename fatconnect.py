#! /usr/bin/python3

from fatsecret import Fatsecret
from .connect import Connect

class FatConnect(Connect):
    @Connect.with_connection
    def connect(self):
        self.fs = Fatsecret(self.client_id, self.client_secret, session_token = self.token)

    def info_check(self):
        check = self.fs.profile_get()
        print(check)

    def get_permission_url(self):
        self.fs = Fatsecret(self.client_id,self.client_secret)
        return self.fs.get_authorize_url()

    @Connect.with_connection
    def get_permission_token(self, params):
        token = self.fs.authenticate(params['postVerify'])
        return token

    def set_day(self, day):
        self.day = day

    def sum_day(self, day):
        dic=dict()
        for i in ['calories','carbohydrate','protein','fat','fiber','sodium']:
            l=[]
            for d in day:
                if i in d.keys():
                    l.append(float(d[i]))
            dic[i] = sum(l)
        return dic

    def get_day_data(self):
        return self.fs.food_entries_get(date = self.day)

    def get_month_from(self, date):
        m = fs.food_entries_get_month(date = date)
        print(m)

    def get_data(self, day):
        self.set_day(day)
        data = self.get_day_data()
        return self.sum_day(data)
