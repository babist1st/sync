#! /usr/bin/python3

from fitbit import Fitbit
from .connect import Connect

class FitConnect(Connect):
    @Connect.with_connection
    def connect(self):
        self.f = Fitbit(self.client_id, self.client_secret,
                        access_token = self.token['access_token'],
                        expires_at = self.token['expires_at'],
                        refresh_token = self.token['refresh_token'],
                        refresh_cb = self.refresh_cb)

    def info_check(self):
        profile = self.f.user_profile_get()
        print("Connected profile", profile['user']['fullName'])

    def get_permission_url(self):
        self.f = Fitbit(self.client_id, self.client_secret)
        return self.f.client.authorize_token_url()[0]

    @Connect.with_connection
    def get_permission_token(self, code):
        self.f.client.fetch_access_token(code['code'][0])
        return self.f.client.session.token

    def get_day_data(self, day):
        return self.f.foods_log(date=day)

    def delete_day_data(self, logid):
        return self.f.delete_foods_log(logid)

    def refresh_cb(self, token, *args, **kwargs):
        print('Updated token')
        self.token = token
        self.write()
